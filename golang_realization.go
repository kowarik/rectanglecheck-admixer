package main

import (
	"fmt"
	"math"
)

func main() {
	var points = [20][2]float64{
		{1, 1},
		{1, 4},
		{6, 1},
		{6, 4},
		{2, 3},
		{2, 6},
		{5, 3},
		{5, 6},
		{5, 4},
		{6, 3},
		{2, 1},
		{5, 1},
		{0, 0},
		{2.25, 0},
		{2.25, 2.25},
		{0, 2.25},
		{0.45, 0.45},
		{-0.05, 0.95},
		{-0.95, 0.05},
		{-0.45, -0.45},
	}
	NumOfRectangle(points)
}

func NumOfRectangle(points [20][2]float64) {
	var numRectangles int = 0
	var pointsLength int = len(points)
	for i := 0; i < pointsLength; i++ {
		for j := 0; j < pointsLength; j++ {
			for k := 0; k < pointsLength; k++ {
				for l := 0; l < pointsLength; l++ {
					if j != i && k != i && k != j && l != i && l != j && l != k {
						if IsRectangle(points[i][0], points[i][1],
							points[j][0], points[j][1],
							points[k][0], points[k][1],
							points[l][0], points[l][1]) {
							numRectangles++
						}
					}
				}
			}
		}
	}
	fmt.Printf("Прямоугольников = %d", numRectangles/24)
}

func IsRectangle(x1, y1, x2, y2, x3, y3, x4, y4 float64) bool {
	if GetLength(x1, y1, x2, y2) == GetLength(x3, y3, x4, y4) && GetLength(x1, y1, x2, y2) > 0 {
		if GetLength(x1, y1, x3, y3) == GetLength(x2, y2, x4, y4) && GetLength(x1, y1, x3, y3) > 0 {
			if GetLength(x1, y1, x4, y4) == GetLength(x2, y2, x3, y3) && GetLength(x1, y1, x4, y4) > 0 {
				return true
			}
		}
	}
	return false
}

func GetLength(x1, y1, x2, y2 float64) float64 {
	//return math.Round(math.Sqrt(math.Pow(x2-x1, 2)+math.Pow(y2-y1, 2))*10) / 10 //точность до 1 знака после запятой
	var n int = 1 //точность до n знаков после запятой
	return Round(math.Sqrt(math.Pow(x2-x1, 2)+math.Pow(y2-y1, 2)), n)
}

func Round(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(math.Round(num*output)) / output
}
