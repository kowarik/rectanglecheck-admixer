﻿using System;

namespace Test1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] points = new double[,] {
                { 1, 1 },
                { 1, 4 },
                { 6, 1 },
                { 6, 4 },
                { 2, 3 },
                { 2, 6 },
                { 5, 3 },
                { 5, 6 },
                { 5, 4 },
                { 6, 3 },
                { 2, 1 },
                { 5, 1 },
                { 0, 0 },
                { -0.4, 0.8 },
                { 1.2, 1.6 },
                { 1.6, 0.8 },
                { 1.2, 1.6 }
            };

            NumOfRectangle(points);
        }

        private static void NumOfRectangle(double[,] points)
        {
            int numRectangles = 0;
            int pointsLength = points.GetLength(0);
            for (int i = 0; i < pointsLength; i++)
                for (int j = 0; j < pointsLength; j++)
                    for (int k = 0; k < pointsLength; k++)
                        for (int l = 0; l < pointsLength; l++)
                            if (j != i && k != i && k != j && l != i && l != j && l != k)
                                if (IsRectangle((points[i, 0], points[i, 1]),
                                                (points[j, 0], points[j, 1]),
                                                (points[k, 0], points[k, 1]),
                                                (points[l, 0], points[l, 1])))
                                    numRectangles++;
            Console.WriteLine("Прямоугольников: " + numRectangles / 24);
        }

        private static bool IsRectangle((double x, double y) p1,
                                        (double x, double y) p2,
                                        (double x, double y) p3,
                                        (double x, double y) p4)
        {
            //проверка на равность расстояния между точками (стороны и диагональ)
            if (GetLength(p1, p2) == GetLength(p3, p4) && GetLength(p1, p2) > 0)
                if (GetLength(p1, p3) == GetLength(p2, p4) && GetLength(p1, p3) > 0)
                    if (GetLength(p1, p4) == GetLength(p2, p3) && GetLength(p1, p4) > 0)
                        return true;
            return false;
        }

        private static double GetLength((double x, double y) p1,
                                        (double x, double y) p2)
        {
            return Math.Round(Math.Sqrt(Math.Pow(p2.x - p1.x, 2) + Math.Pow(p2.y - p1.y, 2)), 10);
        }
    }
}
